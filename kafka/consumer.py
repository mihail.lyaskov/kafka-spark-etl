from kafka import KafkaConsumer
import json
#connect to Kafka server and pass the topic we want to consume
consumer = KafkaConsumer('test', bootstrap_servers=['192.168.1.2:9092'])

for msg in consumer:
	value = msg[6]
	data = json.loads(value)
	print data['timestamp']
