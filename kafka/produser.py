
#!/usr/bin/python
import argparse , time
from time import gmtime, strftime
from kafka import KafkaProducer

data1 = '{ "device" : "TestDev" , "power" : 123.4 , "temperature" : 23.1 , "timestamp" : "'
data2 = '"}'

def main(args):
	produser = KafkaProducer(bootstrap_servers=args.server)
	while(True):
		payload = data1 + str(strftime("%d-%m-%Y %H:%M:%S", gmtime())) + data2
		print payload
		produser.send(topic=args.topic, value=payload)
		time.sleep(2)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Start kafka produser.')
	parser.add_argument('--server', type=str, required=True, help='String with server address and port <server>:<port>')
	parser.add_argument('--topic', type=str, required=True, help='Topic to send data to')

	args = parser.parse_args()
	main(args)

