#!/bin/bash
KAFKA=192.168.1.104:9092
TOPIC=test
PARQUET=parquetData.parquet
CHECKPOINT=checkpoint

spark-submit --packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0 kafka_to_parquet_hdfs_etl.py --server $KAFKA --topic $TOPIC --parquetDir $PARQUET --checkpointDir $CHECKPOINT