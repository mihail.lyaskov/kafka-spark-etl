
from __future__ import print_function

import argparse

from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *

def parseArgs():
  parser = argparse.ArgumentParser(description='This is a structured streaming application that consumes kafka topics \
    and saves their value into parquet table to HDFS.')
  parser.add_argument('--server', type=str, required=True, help='String with server address and port <server>:<port>')
  parser.add_argument('--topic', type=str, required=True, help='Topic to read data from')
  parser.add_argument('--offset', type=str, required=False , default='latest', help='offset can be earliest or latest')
  parser.add_argument('--parquetDir', type=str, required=True , help='parquet hdfs dir')
  parser.add_argument('--checkpointDir', type=str, required=True , help='checkpoint hdfs dir')

  args = parser.parse_args()
  return args


def main(args):

    schema = StructType() \
        .add("device",StringType()) \
        .add("power",DoubleType()) \
        .add("temperature",DoubleType()) \
        .add("timestamp",StringType())

    spark = SparkSession\
        .builder\
        .appName("StreamingJob")\
        .getOrCreate()

    # Create DataSet representing the stream of input lines from kafka
    kafka = spark\
        .readStream\
        .format("kafka")\
        .option("kafka.bootstrap.servers", args.server)\
        .option("subscribe", args.topic)\
        .option("startingOffsets", args.offset) \
        .load()

    # Parse JSON
    data = kafka\
        .select(from_json(col("value").cast("string"), schema).alias("data")) \
        .select("data.*")
    
    # Start running the query that prints the running counts to the console

    result = data\
        .writeStream \
        .format('parquet')\
        .option("path", args.parquetDir) \
        .partitionBy("timestamp") \
        .trigger(processingTime="10 seconds") \
        .option("checkpointLocation", args.checkpointDir) \
        .start()

    result.awaitTermination()
    

if __name__ == "__main__":
  main(parseArgs())

    