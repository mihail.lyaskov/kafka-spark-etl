from __future__ import print_function
import argparse
from pyspark.sql import SparkSession
from pyspark.sql import Row

def parseArgs():
  parser = argparse.ArgumentParser(description='This application shows the contents of parquet table.')
  parser.add_argument('--dir', type=str, required=True, help='Directory')
  args = parser.parse_args()
  return args

def parquet(spark,args):
    parquetFile = spark.read.parquet(args.dir)

    # Parquet files can also be used to create a temporary view and then used in SQL statements.
    parquetFile.createOrReplaceTempView("parquetFile")
    data = spark.sql("SELECT * FROM parquetFile")
    data.show()


if __name__ == "__main__":
    spark = SparkSession \
        .builder \
        .appName("Python Spark SQL data source") \
        .getOrCreate()

    parquet(spark,parseArgs())
    spark.stop()